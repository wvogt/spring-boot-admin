# Supported tags and respective `Dockerfile` links
 - `1.5.0`, `latest` ([_Dockerfile_](https://bitbucket.org/wvogt/spring-boot-admin/src/e7ef7f05ee7a58a1b2e14439295af1ba7dce88b4/Dockerfile?at=master))
 - `1.4.6`, `1.4` ([_Dockerfile_](https://bitbucket.org/wvogt/spring-boot-admin/src/e7ef7f05ee7a58a1b2e14439295af1ba7dce88b4/Dockerfile?at=1.4))

Follow this link ([codecentric/spring-boot-admin](https://github.com/codecentric/spring-boot-admin)) to learn more about the application.